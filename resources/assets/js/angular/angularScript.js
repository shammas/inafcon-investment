/**
 * Created on 02/03/19.
 */

var app = angular.module('inafcon', ['ngRoute', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngFileUpload', 'cp.ngConfirm', 'angular-loading-bar']);
app.config(['$routeProvider', '$locationProvider', 'cfpLoadingBarProvider', function ($routeProvider, $locationProvider, cfpLoadingBarProvider) {
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'dashboard/dashboard'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard/dashboard',
            controller: 'DashboardController'
        })
        .when('/testimonial', {
            templateUrl: 'dashboard/testimonial',
            controller: 'TestimonialController'
        })
       .when('/client', {
            templateUrl: 'dashboard/client',
            controller: 'ClientController'
        })
       .when('/project_cat', {
            templateUrl: 'dashboard/project_cat',
            controller: 'Project_catController'
        })
        .when('/project', {
            templateUrl: 'dashboard/project',
            controller: 'ProjectController'
        })
        .when('/gallery_cat', {
            templateUrl: 'dashboard/gallery_cat',
            controller: 'Gallery_catController'
        })
        .when('/gallery', {
            templateUrl: 'dashboard/gallery',
            controller: 'GalleryController'
        })
}]);

//Pagination filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

