/**
 * Created on 04/03/19.
 */


app.controller('ProjectController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.projects = [];
        $scope.newproject = {};
        $scope.curproject = false;
        $scope.files = [];
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = [];
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadProject();

        function loadProject() {
            $http.get($rootScope.base_url + 'dashboard/project/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.projects = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }
        $http.get($rootScope.base_url + 'Project_cat_Controller').then(function (response) {
            if (response.data) {
                $scope.categories = response.data;
            }
        });

        $scope.newProject = function () {
            $scope.newproject = {};
            $scope.filespre = [];
            $scope.uploaded = [];
            $scope.files = [];
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
            $scope.curproject = false;
            $scope.validationError = [];
        };

        $scope.editProject = function (item) {
            $scope.showform = true;
            $scope.curproject = item;
            $scope.newproject = angular.copy(item);
            $scope.newproject.date= new Date(item.date);
            $scope.files = [];
            $scope.uploaded = [];
            $scope.validationError = [];
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addProject = function () {

            $scope.newproject.date = $filter('date')($scope.date, "yyyy-MM-dd");

            var fd = new FormData();

            angular.forEach($scope.newproject, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));
           
            if ($scope.newproject['id']) {
                var url = $rootScope.base_url + 'dashboard/project/edit/' + $scope.newproject.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadProject();
                        $scope.newproject = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if (1) {
                    var url = $rootScope.base_url + 'dashboard/project/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadProject();
                            $scope.newproject = {};
                            $scope.showform = false;
                            $scope.files = [];

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.validationError.file = response.data;
                            $scope.files = [];

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteProject = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/project/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.projects.indexOf(item);
                                    $scope.projects.splice(index, 1);
                                    loadProject();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles,type) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                $scope.files.push(file);
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/project/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded.push(response.data);
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/project/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    var index = $scope.curproject.files.indexOf(item);
                    $scope.curproject.files.splice(index, 1);
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        

        $scope.showProjectFiles = function (item) {
            console.log(item);
            $scope.projectfiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (project, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return project;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

