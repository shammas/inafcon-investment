/**
 * Created on 02/03/19.
 */


app.controller('ClientController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.clients = [];
        $scope.newclient = {};
        $scope.curclient = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = null;
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadClient();

        function loadClient() {
            $http.get($rootScope.base_url + 'dashboard/client/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.clients = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newClient = function () {
            $scope.newclient = {};
            $scope.filespre = [];
            $scope.uploaded = null;
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
            $scope.curclient = false;
            $scope.validationError = [];
        };

        $scope.editClient = function (item) {
            $scope.showform = true;
            $scope.curclient = item;
            $scope.newclient = angular.copy(item);
            $scope.files = false;
            $scope.validationError = [];
        };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addClient = function () {

            var fd = new FormData();

            angular.forEach($scope.newclient, function (item, key) {
                fd.append(key, item);
            });

            fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newclient['id']) {
                var url = $rootScope.base_url + 'dashboard/client/edit/' + $scope.newclient.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadClient();
                        $scope.newclient = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
                if (1) {
                    var url = $rootScope.base_url + 'dashboard/client/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadClient();
                            $scope.newclient = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.validationError.file = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
                } else {
                    $scope.validationError.file = false;
                }
            }
        };

        $scope.deleteClient = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/client/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.clients.indexOf(item);
                                    $scope.clients.splice(index, 1);
                                    loadClient();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                console.log(file);
                $scope.files = file;
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/client/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded = response.data;
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage = function (item) {

            var url = $rootScope.base_url + 'dashboard/client/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    $scope.item_files = '';
                }, function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showClientFiles = function (item) {
            console.log(item);
            $scope.clientfiles = item;
        };


        /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (client, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return client;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

