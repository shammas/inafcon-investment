/**
 * Created  on 02/03/19.
 */


app.controller('Gallery_catController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.gallery_cats = [];
        $scope.newgallery_cat = {};
        $scope.curgallery_cat = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadGallery_cat();

        function loadGallery_cat() {
            $http.get($rootScope.base_url + 'dashboard/gallery_cat/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.gallery_cats = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newGallery_cat = function () {
            $scope.newgallery_cat = {};
            $scope.filespre = [];
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            
        };

        $scope.editGallery_cat = function (item) {
            $scope.showform = true;
            $scope.curgallery_cat = item;
            $scope.newgallery_cat = angular.copy(item);
             };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addGallery_cat = function () {

            var fd = new FormData();

            angular.forEach($scope.newgallery_cat, function (item, key) {
                fd.append(key, item);
            });

            if ($scope.newgallery_cat['id']) {
                var url = $rootScope.base_url + 'dashboard/gallery_cat/edit/' + $scope.newgallery_cat.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadGallery_cat();
                        $scope.newgallery_cat = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
               
                    var url = $rootScope.base_url + 'dashboard/gallery_cat/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadGallery_cat();
                            $scope.newgallery_cat = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
            }
        };

        $scope.deleteGallery_cat = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/gallery_cat/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.gallery_cats.indexOf(item);
                                    $scope.gallery_cats.splice(index, 1);
                                    loadGallery_cat();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

 /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (gallery_cat, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return gallery_cat;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

