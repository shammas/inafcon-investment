<?php
class Index extends CI_Controller { 
	 
        protected $header = 'templates/header';
        protected $footer = 'templates/footer';

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Client_model', 'client');
            $this->load->model('Gallery_model', 'gallery');
            $this->load->model('Project_model', 'project');
            $this->load->model('Project_cat_model', 'project_cat');
            $this->load->model('Project_file_model', 'project_file');
        }

        protected $current = '';

        public function index()
        {
            $data['testimonials'] = $this->testimonial->get_all();
            $data['clients'] = $this->client->get_all();

            $this->current = 'index';
            $this->load->view($this->header, ['current' => $this->current]);
            $this->load->view('index',$data);
            $this->load->view($this->footer);
        }

        public function about()
        {
            $this->current = 'about';
            $this->load->view($this->header, ['current' => $this->current]);
            
            $this->load->view('about');
            $this->load->view($this->footer);
        }

        public function clients()
        {
            $data['clients'] = $this->client->get_all();

            $this->current = 'clients';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('clients',$data);
            $this->load->view($this->footer);
        }

        public function contact()
        {
            $this->current = 'contact';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('contact');
            $this->load->view($this->footer);
        }

        public function gallery()
        {
            $data['galleries'] = $this->gallery->with_gallery_cat()->get_all();
            $this->current = 'gallery';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('gallery',$data);
            $this->load->view($this->footer);
        }

        public function projects()
        {
            $data['projects'] = $this->project->with_project_cat()->with_files()->get_all();
            $this->current = 'projects';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('projects',$data);
            $this->load->view($this->footer);
        }
        public function projects_single($id)
        {
            $data['next'] = $this->project->next($id);
            $data['previous'] = $this->project->previous($id);

            $data['projects'] = $this->project->with_project_cat()->with_files()->where('id',$id)->get_all();
            if($data['projects'] == false){
                show_404(); 
            }
            $this->current = 'projects-single';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('projects-single',$data);
            $this->load->view($this->footer);
        }
        public function services()
        {
            $this->current = 'services';
            $this->load->view($this->header, ['current' => $this->current]);

            $this->load->view('services');
            $this->load->view($this->footer);
        }
        public function test()
        {
            $this->load->view('test');
        }
        public function testimonial()
        {
            $data['testimonials'] = $this->testimonial->get_all();
            
            $this->current = 'testimonial';
            $this->load->view($this->header,['current' => $this->current]);

            $this->load->view('testimonial',$data);
            $this->load->view($this->footer);
        }
}