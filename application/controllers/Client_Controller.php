<?php
/**
 * Client_Controller.php
 * Date: 02/03/19
 * Time: 12:30 PM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Client_Controller extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Client_model', 'client');

        $this->load->library(['upload', 'image_lib','ion_auth']);

        $this->load->library('form_validation');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }
    }

    function index()
    {
        $data = $this->client->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));

    }

    function get_all()
    {
        $data = $this->client->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);

            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                $post_data['file_name'] = $uploaded->file_name;
                $post_data['url'] = base_url() . 'uploads/clients/';

                $client_id = $this->client->insert($post_data);
                
                if ($client_id) {
                    if (!is_dir(getwdir().'uploads/clients/thumb')) {
                        mkdir(getwdir() . 'uploads/clients/thumb', 0777, TRUE);
                    }

                    /*****Create Thumb Image****/
                    $img_cfg['source_image'] = getwdir() . 'uploads/clients/' . $uploaded->file_name;
                    $img_cfg['maintain_ratio'] = TRUE;
                    $img_cfg['new_image'] = getwdir() . 'uploads/clients/thumb/' . $uploaded->file_name;
                    $img_cfg['quality'] = 99;
                    $img_cfg['master_dim'] = 'height';
                    $img_cfg['height'] = 50;

                    $resize_error = [];
                    $this->image_lib->initialize($img_cfg);
                    if (!$this->image_lib->resize()) {
                        $resize_error[] = $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();

                    /********End Thumb*********/

                    /*resize and create thumbnail image*/
                    if ($uploaded->file_size > 1024) {
                        $img_cfg['image_library'] = 'gd2';
                        $img_cfg['source_image'] = getwdir() . 'uploads/clients/' . $uploaded->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getwdir() . 'uploads/clients/' . $uploaded->file_name;
                        $img_cfg['height'] = 500;
                        $img_cfg['quality'] = 100;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End resize*********/
                    }
                }
                if (empty($resize_error)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                } else {

                    $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                }
            }else{
                $this->output->set_status_header(402, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));
            }
        }
    }

    
    function update($id)
    {
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
            $current_file = $this->client->get();

            unset($post_data['uploaded']);
            
            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                $post_data['file_name'] = $uploaded->file_name;
                $post_data['url'] = base_url() . 'uploads/clients/';
               
                if ($this->client->update($post_data,$id)) {
                    /*****Create Thumb Image****/
                    $img_cfg['source_image'] = getwdir() . 'uploads/clients/' . $uploaded->file_name;
                    $img_cfg['maintain_ratio'] = TRUE;
                    $img_cfg['new_image'] = getwdir() . 'uploads/clients/thumb/' . $uploaded->file_name;
                    $img_cfg['quality'] = 99;
                    $img_cfg['height'] = 50;
                    $img_cfg['master_dim'] = 'height';

                    $this->image_lib->initialize($img_cfg);
                    if (!$this->image_lib->resize()) {
                        $resize_error[] = $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();

                    /********End Thumb*********/

                    /*resize and create thumbnail image*/
                    if ($uploaded->file_size > 1024) {
                        $img_cfg['image_library'] = 'gd2';
                        $img_cfg['source_image'] = getwdir() . 'uploads/clients/' . $uploaded->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getwdir() . 'uploads/clients/' . $uploaded->file_name;
                        $img_cfg['height'] = 500;
                        $img_cfg['quality'] = 100;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End resize*********/
                    }
                }
                $resize_error = [];
                if (empty($resize_error)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                }
            } elseif($this->client->update($post_data,$id)) {
                $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
            }else {
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['validation_error' => 'Please select images.']));
            }
        }
    }

    

    function upload()
    {
        if (!is_dir(getwdir().'uploads/clients')) {
            mkdir(getwdir() . 'uploads/clients', 0777, TRUE);
        }
        $config['upload_path'] = getcwd() . '/uploads/clients';
        $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
        $config['max_size'] = 4096;
        $config['file_name'] = date('YmdHis');

        $this->upload->initialize($config);
        if ($this->upload->do_upload('file')) {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
        }else{
            $this->output->set_status_header(401, 'File Upload Error');
            $this->output->set_content_type('application/json')->set_output($this->upload->display_errors('',''));
        }
    }



    public function delete($id)
    {
        $client = $this->client->with_file()->where('id', $id)->get();
        if ($client) {
            $this->client->delete($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Client Deleted']));
        } else {
            $this->output->set_status_header(500, 'Validation error');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


}