<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add project</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addProject()" >
                       <div class="form-group">
                            <label class="col-lg-1 control-label">Title</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.title}">
                                <input type="text" placeholder="Project Title here" class="form-control" ng-model="newproject.title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Project Category</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.category_id}">
                            <select name="category" ng-model=newproject.category_id class="form-control">
                                    <option value="" selected>Select category</option>
                                    <option  ng-repeat="category in categories" value="{{category.id}}">
                                    {{category.project_cat}}
                                    </option>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Description</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.description}">
                                <textarea name="description" class="form-control" placeholder="Type here" ng-model="newproject.description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-lg-1">Date</label>
                            <div class="col-lg-11">
                                <input type="date" class="form-control" name="date" ng-model= "date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Client</label>
                            <div class="col-lg-5"  ng-class="{'has-error' : validationError.client}">
                                <input type="text" placeholder="Client Name Here" class="form-control" ng-model="newproject.client">
                            </div>
                            <label class="col-lg-1 control-label">Place</label>
                            <div class="col-lg-5"  ng-class="{'has-error' : validationError.place}">
                                <input type="text" placeholder="Project Place" class="form-control" ng-model="newproject.place">
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error' : validationError.file}">
                            <label for="" class="control-label col-lg-1">Photo</label>
                            <div class="col-md-11">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        accept="image/*"
                                        ngf-max-height="5000"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'" ngf-multiple="false"
                                        ngf-pattern="'image/*'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit"  ng-bind="(curproject == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                  
                    <div class="lightBoxGallery" ng-show="curproject">
                        <div ng-repeat="file in curproject.files" class="col-md-2">
                            <a class="example-image-link" href="{{file.url + file.file_name}}" data-lightbox="item-list-{{curmoment.name}}" data-title="" >
                                <img src="{{file.url + file.file_name}}" width="50px">
                            </a>
                            <a ng-click="deleteImage(file)" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>

        <div class="col-lg-3" ng-show="files">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div ng-repeat="file in files">
                        <h5>{{files.name}}</h5>
                        <div class="lightBoxProject">
                            <a class="example-image-link" href="{{file.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="file.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{file.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{file.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{file.name}} {{file.$error}} {{file.$errorParam}}.</p>
                </div>

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All projects</h5>
                    <div class="ibox-tools">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="">
                        <button type="button" class="btn btn-primary" ng-click="newProject()">
                            Add a new project
                        </button>
                    </div>
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="dataTables_length" id="DataTables_Table_0_length">
                                <label>
                                    <select  aria-controls="DataTables_Table_0" class="form-control input-sm" ng-model="numPerPage"
                                             ng-options="num for num in paginations">{{num}}
                                    </select>
                                    entries
                                </label>
                            </div>
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                <label>Search:<input class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0" type="search"></label>
                            </div>
                            <table class="table table-striped table-bordered table-hover dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                                <thead>
                                <tr role="row">
                                    <th>Sl No</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Description</th>
                                    <th>Client</th>
                                    <th>Date</th>
                                    <th>Place</th>
                                    <th>Images</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeA odd" role="row" dir-paginate="project in projects | filter:search | limitTo:pageSize | itemsPerPage:numPerPage" current-page="currentPage">

                                    <td>{{$index+1}}</td>
                                    <td>{{project.title}}</td>
                                    <td>{{project.project_cat.project_cat}}</td>
                                    <td>{{project.description}}</td>
                                    <td>{{project.client}}</td>
                                    <td>{{project.date | date:'dd-MM-yyyy'}}</td>
                                    <td>{{project.place}}</td>
                                    <td class="center">
                                        <a class="example-image-link" href="{{image.url+image.file_name}}" data-lightbox="images-{{project.id}}" data-title="" ng-repeat="image in project.files">
                                            <img src="{{image.url+image.file_name}}" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                                        </a>
                                    </td>
                                    <td class="center">
                                        <div  class="btn-group btn-group-xs" role="group">
                                            <button type="button" class="btn btn-info" ng-click="editProject(project)">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deleteProject(project)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-4">

                                <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                    Showing {{currentPage}} to {{(numPerPage < projects.length  ? currentPage*numPerPage :projects.length)}} of {{projects.length}} entries
                                </div>
                                <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                    Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{projects.length}} entries
                                </div>
                            </div>



                            <div class="col-md-8 pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true">
                                    </dir-pagination-controls>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
