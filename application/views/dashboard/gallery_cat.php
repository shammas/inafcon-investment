<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add Category</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addGallery_cat()" >
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Gallery Category</label>
                            <div class="col-lg-8"  ng-class="{'has-error' : validationError.gallery_cat}">
                                <input name="gallery_cat" class="form-control" placeholder="Gallery Category" ng-model="newgallery_cat.gallery_cat">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit"  ng-bind="(curgallery_cat == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All Gallery Categories</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="">
                        <button type="button" class="btn btn-primary" ng-click="newGallery_cat()">
                            Add a new category
                        </button>
                    </div>
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="dataTables_length" id="DataTables_Table_0_length">
                                <label>
                                    <select  aria-controls="DataTables_Table_0" class="form-control input-sm" ng-model="numPerPage"
                                             ng-options="num for num in paginations">{{num}}
                                    </select>
                                    entries
                                </label>
                            </div>
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                               <label>Search:
                                <input class="form-control input-sm" placeholder="" aria-controls="ataTables_Table_0" type="search" ng-model="search">
                                </label>
                            </div>
                            <table class="table table-striped table-bordered table-hover dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                                <thead>
                                <tr role="row">
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 181.217px;" aria-sort="ascending" >Sl No</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189.75px;">Gallery Category</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 151.383px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeA odd" role="row" dir-paginate="gallery_cat in gallery_cats | filter:search | limitTo:pageSize | itemsPerPage:numPerPage" current-page="currentPage">

                                    <td class="sorting_1">{{$index+1}}</td>
                                    <td>{{gallery_cat.gallery_cat}}</td>
                                    <td class="center">
                                        <div  class="btn-group btn-group-xs" role="group">
                                            <button type="button" class="btn btn-info" ng-click="editGallery_cat(gallery_cat)">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deleteGallery_cat(gallery_cat)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-4">

                                <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                    Showing {{currentPage}} to {{(numPerPage < gallery_cats.length  ? currentPage*numPerPage :gallery_cats.length)}} of {{gallery_cats.length}} entries
                                </div>
                                <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                    Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{gallery_cats.length}} entries
                                </div>
                            </div>



                            <div class="col-md-8 pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true">
                                    </dir-pagination-controls>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
