<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Cloudbery Solutions">
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Inafcon investments PVT.LTD</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link rel="icon" href="../images/favicon.png">
    <!-- =========== Style Sheets =========== -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/content-box.css">
    <link rel="stylesheet" href="css/image-box.css">
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/components.css">
    <link rel="stylesheet" href="css/flexslider/flexslider.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/contact-form.css">
    <link rel="stylesheet" href="css/social.stream.css">
    <link rel="stylesheet" href="css/skin.css">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.css">
</head>
<body>
    <div id="preloader"></div>
    <header class="fixed-top scroll-change" data-menu-anima="fade-in">
        <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
            <div class="navbar-mini scroll-hide">
                <div class="container">
                    <div class="nav navbar-nav navbar-left">
                        <span><i class="fa fa-phone"></i>1-800-405-377</span>
                        <hr />
                        <span><i class="fa fa-envelope"></i>info@inafcon.com</span>
                        <hr />
                        <span>  <i class="fa fa-map-marker"></i>Collins Street 8007, KSA</span>
                        <hr />
                        <span><i class="fa fa-calendar"></i>Mon - Sat: 8.00 - 19:00</span>
                    </div>
                    <div class="nav navbar-nav navbar-right">
                        <div class="minisocial-group">
                            <a target="_blank" href="#"><i class="fa fa-facebook first"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-instagram"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-youtube"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-main">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="index">
                            <img class="logo-default" src="img/logo.png" alt="logo" />
                            <img class="logo-retina" src="img/logo-retina.png" alt="logo" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index">Home</a></li>
                            <li class="active"><a href="about">About Us</a></li>
                            <li><a href="clients">Our Clients</a></li>
                            <li><a href="servive">Our Services</a></li>
                            <li><a href="projects">Major Projects</a></li>
                            <li><a href="testimonial">Costomer's Says</a></li>
                            <li><a href="gallery">Our Gallery</a></li>
                            <li><a href="contact">Contact us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Title base</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="#">Home</a></li>
                        <li class="active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>





    
    

    <i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>
    <footer class="footer-base">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footer-center text-left">
                        <img width="120" src="../images/logo.png" alt="" />
                        <hr class="space m" />
                        <p class="text-s">Collins Street West 8007, San Fransico, United States.</p>
                        <div class="tag-row text-s">
                            <span>support@inafcon.com</span>
                            <span>+02 3205550678</span>
                        </div>
                        <hr class="space m" />
                        <div class="btn-group social-group btn-group-icons">
                            <a target="_blank" href="#" data-social="share-facebook">
                                <i class="fa fa-facebook text-xs circle"></i>
                            </a>
                            <a target="_blank" href="#" data-social="share-twitter">
                                <i class="fa fa-twitter text-xs circle"></i>
                            </a>
                            <a target="_blank" href="#" data-social="share-google">
                                <i class="fa fa-google-plus text-xs circle"></i>
                            </a>
                            <a target="_blank" href="#" data-social="share-linkedin">
                                <i class="fa fa-linkedin text-xs circle"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 footer-left text-left">
                        <div class="row">
                            <div class="col-md-6 text-s">
                                <h3>Quick Links</h3>
                                <a href="index">Home</a><br />
                                <a href="about">About Inafcon</a><br />
                                <a href="testimonial">Costomer's Says</a><br />
                                <a href="clients">Our Clients</a><br />
                                <a href="contact">Contact Us</a><br />
                            </div>
                            <div class="col-md-6 text-s">
                                <h3>Our Company</h3>
                                <a href="services">Our Sercices</a><br />
                                <a href="gallery">Our Gallery</a><br />
                                <a href="projects">Our Projects</a><br />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 footer-left text-left">
                        <h3>You can trust us</h3>
                        <p class="text-s">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row copy-row">
                <div class="container">
                    <div class="col-md-12 copy-text">
                        © 2018 Inafcon investments PVT.LTD <span class="float-right">Designed and Maintained by <a href="http://cloudbery.com" target="_blank"><img src="img/cloudbery.png"></a></span>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/script.js"></script>
        <script async src="js/bootstrap.min.js"></script>
        <script src="js/imagesloaded.min.js"></script>
        <script src="js/parallax.min.js"></script>
        <script src="css/flexslider/jquery.flexslider-min.js"></script>
        <script async src="js/isotope.min.js"></script>
        <script async src="js/contact-form.js"></script>
        <script async src="js/jquery.progress-counter.js"></script>
        <script async src="js/jquery.tab-accordion.js"></script>
        <script async src="js/bootstrap.popover.min.js"></script>
        <script async src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/social.stream.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/smooth.scroll.min.js"></script>
    </footer>
</body>
</html>
