﻿
    <div class="section-empty no-paddings">
        <div class="section-slider row-18 white">
            <div class="flexslider advanced-slider slider visible-dir-nav" data-options="animation:fade">
                <ul class="slides">
                    <li data-slider-anima="fade-left" data-time="1000">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('img/slider-1.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner text-left">
                                        <div class="row">
                                            <div class="col-md-8 anima">
                                                <h2 class="text-color">Architecture and business</h2>
                                                <h1 class="text-l text-normal">An architect is a person who plans, designs and reviews the construction of buildings and any other structure</h1>
                                                <hr class="space s" />
                                                <p class="width-500">
                                                    I enjoy art and museums but also churches, anything that gives me insight into the history and soul of the place I'm in.
                                                    I can also be a beach bum. I like to laze in the shade of a palm tree with a good book or float in a warm sea at sundown.
                                                    Check our awesome servive and learn more of this.
                                                </p>
                                            </div>
                                        </div>
                                        <hr class="space visible-sm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('img/slider-2.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner text-left">
                                        <div class="row">
                                            <div class="col-md-8 anima">
                                                <h2 class="text-color">Architecture and business</h2>
                                                <h1 class="text-l text-normal">An architect is a person who plans, designs and reviews the construction of buildings and any other structure</h1>
                                                <hr class="space s" />
                                                <p class="width-500">
                                                    I enjoy art and museums but also churches, anything that gives me insight into the history and soul of the place I'm in.
                                                    I can also be a beach bum. I like to laze in the shade of a palm tree with a good book or float in a warm sea at sundown.
                                                    Check our awesome servive and learn more of this.
                                                </p>
                                            </div>
                                        </div>
                                        <hr class="space visible-sm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li data-slider-anima="fade-left" data-time="1000">
                        <div class="section-slide">
                            <div class="bg-cover" style="background-image:url('img/slider-3.jpg')">
                            </div>
                            <div class="container">
                                <div class="container-middle">
                                    <div class="container-inner text-left">
                                        <div class="row">
                                            <div class="col-md-8 anima">
                                                <h2 class="text-color">Architecture and business</h2>
                                                <h1 class="text-l text-normal">An architect is a person who plans, designs and reviews the construction of buildings and any other structure</h1>
                                                <hr class="space s" />
                                                <p class="width-500">
                                                    I enjoy art and museums but also churches, anything that gives me insight into the history and soul of the place I'm in.
                                                    I can also be a beach bum. I like to laze in the shade of a palm tree with a good book or float in a warm sea at sundown.
                                                    Check our awesome servive and learn more of this.
                                                </p>
                                            </div>
                                        </div>
                                        <hr class="space visible-sm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="img-box adv-img adv-img-side-content i-top-right white" data-anima="fade-left" data-trigger="hover">
                        <a class="img-box anima-scale-up anima" href="#">
                            <img src="img/land.jpg" alt="" />
                        </a>
                        <div class="caption">
                            <h2>Land Development</h2>
                            <p>
                                We buy land and service it into residential stands. We partner private land owners, municipalities and state to develop land into residential stands, putting all together technical & constructional services
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="img-box adv-img adv-img-side-content i-top-right white" data-anima="fade-left" data-trigger="hover">
                        <a class="img-box anima-scale-up anima" href="#">
                            <img src="img/construction.jpg" alt="" />
                        </a>
                        <div class="caption">
                            <h2>Building & Construction</h2>
                            <p>
                                We design and construct model houses for our members in our various schemes. We are contracted by other players to design and construct model houses.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="img-box adv-img adv-img-side-content i-top-right white" data-anima="fade-left" data-trigger="hover">
                        <a class="img-box anima-scale-up anima" href="#">
                            <img src="img/equipment.jpg" alt="" />
                        </a>
                        <div class="caption">
                            <h2>Equipment Hire</h2>
                            <p>
                                We have a wide range of equipment for land development, construction and all housing services. Zimbabwe Housing Projects has gained good reputation in connection with good service deliver
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="space" />
            <div class="row ">
                <div class="col-md-2 col-sm-6">
                    <p class="text-m text-light">Committed works</p>
                    <div class="counter-box-simple"><span class="counter text-l text-normal" data-to="1238"></span><span class="text-s"></span></div>
                    <hr class="space l" />
                    <p class="text-m text-light">Total Designed</p>
                    <div class="counter-box-simple"><span class="counter text-l text-normal" data-to="7442"></span><span class="text-s"></span></div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <p class="text-m text-light">Our Projects</p>
                    <div class="counter-box-simple"><span class="counter text-l text-normal" data-to="5561"></span><span class="text-s"></span></div>
                    <hr class="space l" />
                    <p class="text-m text-light">Our Employees</p>
                    <div class="counter-box-simple"><span class="counter text-l text-normal" data-to="9851"></span><span class="text-s"></span></div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <hr class="space visible-sm" />
                    <h4 class="text-normal">Official research team</h4>
                    <p>
                        Zimbabwean indigenous business which becameformaly registered in 2010. Originally its focus was consultancy and began to add other services in the consultancy chain from Town Planning, to Land Survey, Environmemnt Planning.
                    </p>
                    <hr class="space s" />
                    <a href="contact" class="btn btn-border btn-sm">Contact Us</a><span class="space"></span>
                    <a href="projects" class="btn-text">Our Projects</a>
                </div>
                <div class="col-md-4 col-sm-12">
                    <hr class="space visible-sm" />
                    <h4 class="text-normal">About the results</h4>
                    <p>
                        Inafcon Investments is one of the largest housing development organisations in Zimbabwe, with a strong national presence in the major towns and cities in the country. It is a member of the Zimbabwe Property Developers Association.
                    </p>
                    <hr class="space s" />

                    <a href="about" class="btn-text">View more About us</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="flexslider carousel outer-navs" data-options="numItems:3,itemMargin:15,controlNav:true,directionNav:true">
                <ul class="slides">
                <?php
                if (isset($testimonials) and $testimonials) {
                    foreach ($testimonials as $testimonial) {
                    ?>
                    <li>
                        <div class="advs-box niche-box-testimonails-cloud">
                            <p>
                               <?php echo $testimonial->description;?>
                            </p>
                            <div class="name-box vertical-row">
                                <i class="vertical-col fa text-l circle onlycover" style="background-image:url('<?php echo $testimonial->url . $testimonial->file_name;?>')"></i>
                                <h5 class="vertical-col subtitle"><?php echo $testimonial->name;?> <span class="subtxt"><?php echo $testimonial->designation;?></span></h5>
                            </div>
                        </div>
                    </li>
                    <?php 
                    }
                }
                ?>     
                </ul>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="row">
                <div class="col-md-3">
                    <div class="title-base text-left">
                        <hr />
                        <h2>Our Clients</h2>
                        <p>Awesome</p>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="flexslider carousel outer-navs png-boxed png-over text-center" data-options="numItems:5,minWidth:100,itemMargin:30,controlNav:false,directionNav:true">
                        <ul class="slides">
                        <?php
                        if (isset($clients) and $clients) {
                            foreach ($clients as $client) {
                            ?>
                            <li>
                                <a class="img-box" href="#">
                                    <img src="<?php echo $client->url . $client->file_name;?>" alt="<?php echo $client->name;?>">
                                </a>
                            </li>
                            <?php 
                            }
                        }
                        ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



























    