
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Our Clients</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="index">Home</a></li>
                        <li class="active">Our Clients</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty section-item text-center">
        <div class="container content">
            <div class="text-center">
                <div class="row">
                    <?php
                    if (isset($clients) and $clients) {
                        foreach ($clients as $client) {
                        ?>
                        <div class="col-md-3">
                            <a class="img-box" href="#">
                                <img width="70%" src="<?php echo $client->url . $client->file_name;?>" alt="<?php echo $client->name;?>">
                            </a>
                        </div>
                        <?php 
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty text-center section-doc">
        <div class="container content">
            <h4 class="text-normal">Contact Us Now</h4>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <hr class="space xs" />
            <a href="contact" target="_blank" class="circle-button btn btn-sm">Contact Us</a>
        </div>
    </div>

    