<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Cloudbery Solutions">
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Inafcon investments PVT.LTD</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link rel="icon" href="<?php echo base_url();?>img/favicon.png">
    <!-- =========== Style Sheets =========== -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/content-box.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/image-box.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/animations.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/components.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/flexslider/flexslider.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/contact-form.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/social.stream.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/skin.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome/css/font-awesome.css">
</head>
<body>
    <div id="preloader"></div>
    <header class="fixed-top scroll-change" data-menu-anima="fade-in">
        <div class="navbar navbar-default mega-menu-fullwidth navbar-fixed-top" role="navigation">
            <div class="navbar-mini scroll-hide">
                <div class="container">
                    <div class="nav navbar-nav navbar-left">
                        <span><i class="fa fa-phone"></i>+263-779-583-610</span>
                        <hr />
                        <span><i class="fa fa-envelope"></i>mail@inafprojects.com</span>
                        <hr />
                        <span>  <i class="fa fa-map-marker"></i>Borrowdale, Harare, Zimbabwe.</span>
                        <hr />
                        <span><i class="fa fa-calendar"></i>Mon - Sat: 8.00 - 19:00</span>
                    </div>
                    <div class="nav navbar-nav navbar-right">
                        <div class="minisocial-group">
                            <a target="_blank" href="#"><i class="fa fa-facebook first"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-instagram"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-youtube"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-main">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url();?>index">
                            <img class="logo-default" src="img/logo.png" alt="logo" />
                            <img class="logo-retina" src="img/logo-retina.png" alt="logo" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="<?php echo ($current == 'index' ? 'active' :'')?>"><a href="<?php echo base_url();?>index">Home</a></li>
                            <li class="<?php echo ($current == 'about' ? 'active' :'')?>"><a href="<?php echo base_url();?>about">About Us</a></li>
                            <li class="<?php echo ($current == 'clients' ? 'active' :'')?>"><a href="<?php echo base_url();?>clients">Our Clients</a></li>
                            <li class="<?php echo ($current == 'services' ? 'active' :'')?>"><a href="<?php echo base_url();?>services">Our Services</a></li>
                            <li class="<?php echo ($current == 'projects' ? 'active' :'')?>"><a href="<?php echo base_url();?>projects">Major Projects</a></li>
                            <li class="<?php echo ($current == 'testimonial' ? 'active' :'')?>"><a href="<?php echo base_url();?>testimonial">Costomer's Says</a></li>
                            <li class="<?php echo ($current == 'gallery' ? 'active' :'')?>"><a href="<?php echo base_url();?>gallery">Our Gallery</a></li>
                            <li class="<?php echo ($current == 'contact' ? 'active' :'')?>"><a href="<?php echo base_url();?>contact">Contact us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>