<i class="scroll-top scroll-top-mobile show fa fa-sort-asc"></i>
    <footer class="footer-base">
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footer-center text-left">
                        <img width="200" src="<?php echo base_url();?>img/logo.png" alt="" />
                        <hr class="space m" />
                        <p class="text-s">Suite A, New Block3, Sam Levy Village,<br/>Borrowdale, Harare, Zimbabwe.</p>
                        <div class="tag-row text-s">
                            <span>mail@inafprojects.com</span>
                            <span>+263-779-583-610</span>
                        </div>
                        <hr class="space m" />
                        <div class="btn-group social-group btn-group-icons">
                            <a target="_blank" href="#" data-social="share-facebook">
                                <i class="fa fa-facebook text-xs"></i>
                            </a>
                            <a target="_blank" href="#" data-social="share-twitter">
                                <i class="fa fa-twitter text-xs"></i>
                            </a>
                            <a target="_blank" href="#" data-social="share-google">
                                <i class="fa fa-google-plus text-xs"></i>
                            </a>
                            <a target="_blank" href="#" data-social="share-linkedin">
                                <i class="fa fa-linkedin text-xs"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 footer-left text-left">
                        <div class="row">
                            <div class="col-md-6 text-s">
                                <h3>Quick Links</h3>
                                <a href="<?php echo base_url();?>index">Home</a><br />
                                <a href="<?php echo base_url();?>about">About Inafcon</a><br />
                                <a href="<?php echo base_url();?>testimonial">Costomer's Says</a><br />
                                <a href="<?php echo base_url();?>clients">Our Clients</a><br />
                                <a href="<?php echo base_url();?>contact">Contact Us</a><br />
                            </div>
                            <div class="col-md-6 text-s">
                                <h3>Our Company</h3>
                                <a href="<?php echo base_url();?>services">Our Sercices</a><br />
                                <a href="<?php echo base_url();?>gallery">Our Gallery</a><br />
                                <a href="<?php echo base_url();?>projects">Our Projects</a><br />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 footer-left text-left">
                        <h3>You can trust us</h3>
                        <p class="text-s">
                            Inafcon Investments is one of the largest housing development organisations in Zimbabwe, with a strong national presence in the major towns and cities in the country
                        </p>
                        <p class="text-s">
                            We are the largest housing development organisations in Zimbabwe, with a strong national presence in the major towns and cities in the country
                        </p>
                    </div>
                </div>
            </div>
            <div class="row copy-row">
                <div class="container">
                    <div class="col-md-12 copy-text">
                        © 2019 Inafcon Investments PVT.LTD <span class="float-right">Design and Maintained by <a href="http://cloudbery.com" target="_blank"><img src="<?php echo base_url();?>img/cloudbery.png"></a></span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/script.js"></script>
    <script async src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/imagesloaded.min.js"></script>
    <script src="<?php echo base_url();?>js/parallax.min.js"></script>
    <script src="<?php echo base_url();?>css/flexslider/jquery.flexslider-min.js"></script>
    <script async src="<?php echo base_url();?>js/isotope.min.js"></script>
    <script async src="<?php echo base_url();?>js/contact-form.js"></script>
    <script async src="<?php echo base_url();?>js/jquery.progress-counter.js"></script>
    <script async src="<?php echo base_url();?>js/jquery.tab-accordion.js"></script>
    <script async src="<?php echo base_url();?>js/bootstrap.popover.min.js"></script>
    <script async src="<?php echo base_url();?>js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url();?>js/social.stream.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>js/smooth.scroll.min.js"></script>
</body>
</html>
