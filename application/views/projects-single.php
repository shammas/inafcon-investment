
    <div class="header-base bg-cover" style="background-image: url(<?php echo base_url();?>img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Our Major Projects</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="<?php echo base_url();?>index">Home</a></li>
                        <li><a href="<?php echo base_url();?>projects">Major Projects</a></li>
                        <li class="active">Projects View</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($projects) and $projects != false) {
        foreach($projects as $project) { 
            
        ?>
    <div class="section-empty section-item">
        <div class="container content">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="grid-list gallery">
                        <div class="grid-box row" data-lightbox-anima="fade-top">
                        <?php 
                        if (isset($project->files) and $project->files != false) {
                            foreach ($project->files as  $file) { ?>
                            <div class="grid-item col-md-4">
                                <a class="img-box i-center" href="<?php echo $file->url . $file->file_name;?>" data-anima="fade-top" data-trigger="hover" data-anima-out="hide">
                                    <i class="fa fa-camera anima"></i>
                                    <img src="<?php echo $file->url . $file->file_name;?>" alt="<?php echo $project->title; ?>" />
                                </a>
                            </div>
                            <?php 
                            }
                        }
                        ?> 
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <p>
                       <?php echo $project->description; ?>
                    </p>
                    <hr class="space xs" />
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-user"></i><?php echo $project->client; ?></li>
                        <li><i class="fa-li fa fa-folder-open-o"></i><?php echo $project->project_cat->project_cat; ?></li>
                        <li><i class="fa-li fa fa-calendar"></i><?php echo date('d / m / Y', strtotime($project->date));?></li>
                        <li><i class="fa-li fa fa-copyright"></i><?php echo $project->place; ?></li>
                    </ul>
                    <hr class="space s" />
                    <div class="btn-group social-group btn-group-icons">
                        <a target="_blank" href="#" data-social="share-facebook">
                            <i class="fa fa-facebook text-xs circle"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-twitter">
                            <i class="fa fa-twitter text-xs circle"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-google">
                            <i class="fa fa-google-plus text-xs circle"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-linkedin">
                            <i class="fa fa-linkedin text-xs circle"></i>
                        </a>
                    </div>
                    <hr class="space m" />
                    <a href="<?php echo base_url();?>contact" class="btn btn-border btn-lg">Contact Now</a>

                </div>
            </div>
            <hr class="space" />
            <div class="row porfolio-bar">
                <div class="col-md-2">
                    <div class="icon-box" data-anima="scale-up" data-trigger="hover">
                    <?php 
                        if (isset($previous) and $previous != false) {
                    ?>
                        <i class="fa fa-arrow-left text-m"></i><label class="text-s"><a href="<?php echo base_url('projects-single/' . $previous->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $previous->title), '-');?>">Previous</a></label>
                    <?php } ?>
                    </div>
                </div>
                <div class="col-md-8 text-center">
                    <a href="<?php echo base_url();?>projects" data-anima="scale-up" data-trigger="hover"><i class="fa fa-th anima"></i></a>
                </div>
                <div class="col-md-2 text-right">
                    <div class="icon-box icon-box-right pull-right" data-anima="scale-up" data-trigger="hover">
                    <?php
                        if (isset($next) and $next != false) {
                    ?>
                        <label class="text-s"><a href="<?php echo base_url('projects-single/' . $next->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $next->title), '-');?>">Next</a></label><i class="fa fa-arrow-right text-m"></i>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    }   
}   
?>
    