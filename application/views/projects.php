
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Our Major Projects</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="index">Home</a></li>
                        <li class="active">Major Projects</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content">
            <div class="maso-list list-sm-6">
                <div class="navbar navbar-inner">
                    <div class="navbar-toggle"><i class="fa fa-bars"></i><span>MENU</span><i class="fa fa-angle-down"></i></div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav over ms-minimal inner maso-filters nav-center">
                            <li class="active"><a data-filter="maso-item">All</a></li>
                            <?php
                            $data = array();
                            if (isset($projects) and $projects != false) {
                                foreach($projects as $pro) {
                                    $cat = $pro->project_cat->project_cat;
                                    if(!in_array($cat, $data)){ ?>
                                    <!-- <li><a data-filter="cat1">Construction</a></li> -->
                                    <li><a data-filter="<?php echo $pro->category_id;?>"><?php echo $cat;?></a></li>
                                    
                                    <?php
                                    array_push($data,$cat);
                            }   }   }
                            ?>
                            <li><a class="maso-order" data-sort="asc"><i class="fa fa-arrow-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="maso-box row" data-lightbox-anima="fade-top">
                    <?php
                    if (isset($projects) and $projects != false) {
                        foreach($projects as $project) { 
                            if (isset($project->files) and $project->files != false) {
                                $i=0;
                            foreach ($project->files as  $file) {
                                $i ++;
                                        }
                        ?>

                        <div data-sort="1" class="maso-item col-md-3 <?php echo $project->category_id; ?>">
                            <div class="img-box adv-img adv-img-down-text">
                                <a class="img-box img-scale-up lightbox i-center" href="<?php echo $file->url . $file->file_name;?>">
                                    <div class="caption">
                                        <i class="fa fa-plus"></i>
                                    </div>
                                    <img src="<?php echo $file->url . $file->file_name;?>" alt="<?php echo $project->title;?>" />
                                </a>
                                <div class="caption-bottom">
                                    <h2><a href="<?php echo base_url('projects-single/' . $project->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $project->title), '-'); ?>"><?php echo $project->title; ?></a></h2>
                                    <p><?php echo $project->place; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php 
                        }   
                    }   }
                    ?>
                    
                   <!--  <div data-sort="12" class="maso-item col-md-3 1">
                        <div class="img-box adv-img adv-img-down-text">
                            <a class="img-box img-scale-up lightbox i-center" href="img/project-1.jpg">
                                <div class="caption">
                                    <i class="fa fa-plus"></i>
                                </div>
                                <img src="img/project-1.jpg" alt="" />
                            </a>
                            <div class="caption-bottom">
                                <h2><a href="projects-single">Boose headphones</a></h2>
                                <p>Tempor incididunt sed</p>
                            </div>
                        </div>
                    </div> -->
                    <div class="clear"></div>
                </div>
                <div class="list-nav">
                    <a href="#" class="circle-button btn btn-sm circle-button load-more-maso" data-pagination-anima="fade-bottom" data-page-items="8">
                        Load More
                        <i class="fa fa-arrow-down"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

   