
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Our Photos</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="index">Home</a></li>
                        <li class="active">Our Photos</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="maso-list gallery">
                <div class="navbar navbar-inner">
                    <div class="navbar-toggle"><i class="fa fa-bars"></i><span>Menu</span><i class="fa fa-angle-down"></i></div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav over ms-minimal inner maso-filters">
                            <li class="current-active active"><a data-filter="maso-item">All</a></li>
                            <?php
                            $data = array();
                            if (isset($galleries) and $galleries != false) {
                                foreach($galleries as $gal) {
                                    $cat = $gal->gallery_cat->gallery_cat;
                                    if(!in_array($cat, $data)){ ?>

                                    <li><a data-filter="<?php echo $gal->category_id;?>"><?php echo $cat;?></a></li>

                                    <?php
                                    array_push($data,$cat);
                            }   }   }
                            ?>
                            <li><a class="maso-order" data-sort="asc"><i class="fa fa-arrow-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="maso-box row" data-lightbox-anima="fade-top">
                    <?php
                    if (isset($galleries) and $galleries != false) {
                        foreach ($galleries as $gallery){
                            if (empty($gallery->video_url)){
                        ?>
                        <div data-sort="1" class="maso-item col-md-4 <?php echo $gallery->category_id;?>">
                            <a class="img-box" href="<?php echo $gallery->url . $gallery->file_name;?>" data-lightbox-anima="fade-top">
                                <img src="<?php echo $gallery->url . $gallery->file_name;?>" alt="<?php echo $gallery->name;?>" />
                            </a>
                        </div>
                        <?php }else { ?>
                        <div data-sort="1" class="maso-item col-md-4 <?php echo $gallery->category_id;?>">
                            <a class="img-box mfp-iframe" href="<?php echo $gallery->video_url;?>" data-lightbox-anima="fade-top">
                                <img src="<?php echo $gallery->url . $gallery->file_name;?>" alt="<?php echo $gallery->name;?>" />
                            </a>
                        </div>
                        <?php 
                        } }
                    }
                    ?>
                    
                    <div class="clear"></div>
                </div>
                <div class="list-nav">
                    <a href="#" class="btn-sm btn load-more-maso" data-page-items="9">Load More <i class="fa fa-arrow-down"></i></a>
                </div>
            </div>
        </div>
    </div>

    