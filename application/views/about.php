
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>About Us</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="#">Home</a></li>
                        <li class="active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content">
            <div class="row">
                <div class="col-md-6 col-center boxed-inverse text-center intro-box" data-anima="fade-in">
                    <p class="text-color text-l">A yellow template with a lot of innovative features.</p>
                    <hr class="space xs" />
                    <h3 class="font-2 text-m text-light ">
                        Modern and clean construction and business template perfect for industrial and professional businesses.
                    </h3>
                    <hr class="space xs" />
                    <a href="#features" class="btn-text scroll-to">Features</a><span class="space"></span>
                    <a href="#" class="btn-text">Buy now</a>
                </div>
            </div>
            <div class="row proporzional-row">
                <div class="col-md-6">
                    <p>
                        Inafcon Investments is one of the largest housing development organisations in Zimbabwe, with a strong national presence in the major towns and cities in the country. It is a member of the Zimbabwe Property Developers Association, Zimbabwe Allied Building Contractors Association (ZABCA) and the Construction Industry Federation of Zimbabwe (CIFOZ). It is in the partnership with the National Building Society for new clients' motgage and ZIMNAT as the insurance partner.
                    </p>
                    <p class="block-quote quote-1">
                        Our Vision is to offer low cost, affordable housing solutions to Zimbabwe and the rest of Africa.
                    </p>
                    <p>
                        Inafcon by origin is a Zimbabwean indigenous business which becameformaly registered in 2010. Originally its focus was consultancy and began to add other services in the consultancy chain from Town Planning, to Land Survey, Environmemnt Planning, and Management to CivilWorks designs. In early 2010, we transformed into any integrated consultancy and began to construct roads, sewer and water supply infrastructure.
                    </p>
                </div>
                <div class="col-md-3 boxed-inverse middle-content text-center">
                    <h4>FOR CREATE AWESOME HOME</h4>
                </div>
                <div class="col-md-3 middle-content boxed white text-center">
                    <h4>FOR CREATE AWESOME HOME</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="row">
                <div class="col-md-4">
                    <div class="advs-box advs-box-top-icon boxed-inverse text-left">
                        <i class="fa fa-bullseye icon circle anima"></i>
                        <h3>Our Mission</h3>
                        <p>
                            Interdum iusto pulvinar consequuntur augue optio repellat fuga hurus expedita tempo est odito.
                            Optio laboriosam! A fugit ea congue. Necessitatibus pede earum aute atque.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="advs-box advs-box-top-icon boxed-inverse text-left">
                        <i class="fa fa-eye icon circle anima"></i>
                        <h3>Our Vision</h3>
                        <p>
                            Interdum iusto pulvinar consequuntur augue optio repellat fuga hurus expedita tempo est odito.
                            Optio laboriosam! A fugit ea congue. Necessitatibus pede earum aute atque.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="advs-box advs-box-top-icon boxed-inverse text-left">
                        <i class="fa fa-diamond icon circle anima"></i>
                        <h3>We Promise</h3>
                        <p>
                            Interdum iusto pulvinar consequuntur augue optio repellat fuga hurus expedita tempo est odito.
                            Optio laboriosam! A fugit ea congue. Necessitatibus pede earum aute atque.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="row vertical-row">
                <div class="col-md-3 col-sm-12">
                    <h1><span class="text-color">Our company</span> <br />is growing fast thanks to you</h1>
                    <p class="text-color">
                        Inafcon by origin is a Zimbabwean indigenous business which becameformaly registered in 2010. Originally its focus was consultancy and began to add other services in the consultancy chain from Town Planning, to Land Survey, Environmemnt Planning, and Management to CivilWorks designs.
                    </p>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <a class="img-box" href="#">
                                <img src="img/long-1.jpg" alt="">
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <a class="img-box" href="#">
                                <img src="img/image-5.jpg" alt="">
                            </a>
                            <ul class="list-texts text-s">
                                <li><b>Location</b>  Suite A, New Block3, Sam Levy Village, Borrowdale, Harare, Zimbabwe.</li>
                                <li><b>Architect</b>   Jason & Perry</li>
                            </ul>
                            <p>
                                Environmemnt Planning, and Management to CivilWorks designs. In early 2010, we transformed into any integrated consultancy
                            </p>
                        </div>
                        <div class="col-md-5 col-sm-12">
                            <p>
                               Inafcon by origin is a Zimbabwean indigenous business which becameformaly registered in 2010. Originally its focus was consultancy and began to add other services in the consultancy chain from Town Planning, to Land Survey, Environmemnt Planning, and Management to CivilWorks designs. 
                            </p>
                            <p>
                                Environmemnt Planning, and Management to CivilWorks designs. In early 2010, we transformed into any integrated consultancy and began to construct roads, sewer and water supply infrastructure.Inaddition we later took advantage of the growing opportunities for building and housing construction opened by cooperatives and other land developers and we assumed a new role of business contractors.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="space s" />
            <p class="block-quote quote-1 quote-gray">
                It is better to lead from behind and to put others in front,
                especially when you celebrate victory when nice things occur.
                You take the front line when there is danger. Then people will appreciate your leadership.
            </p>
        </div>
    </div>

   
   