
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Contact Us</h1>
                        <p>Let's create a great project together! Share your ideas with us.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="index">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="row">
                <div class="col-md-6">
                    <h2>Send a message</h2>
                    <p class="text-color no-margins text-bold">We reply within 48 hour.</p>
                    <hr class="space s" />
                    <p>
                        Let's create a great project together! Share your ideas with us. If you have any questions, don't hesitate to write to us - we'll answer all of them.
                    </p>
                    <hr class="space s" />
                    <form action="http://www.framework-y.com/scripts/php/contact-form.php" class="form-box form-ajax" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <p>Name</p>
                                <input id="name" name="name" placeholder="" type="text" class="form-control form-value" required>
                            </div>
                            <div class="col-md-6">
                                <p>Email</p>
                                <input id="email" name="email" placeholder="" type="email" class="form-control form-value" required>
                            </div>
                        </div>
                        <hr class="space s" />
                        <div class="row">
                            <div class="col-md-12">
                                <p>Messagge</p>
                                <textarea id="messagge" name="messagge" class="form-control form-value" required></textarea>
                                <hr class="space s" />
                                <button class="anima-button btn-border btn-sm btn" type="submit"><i class="fa fa-mail-reply-all"></i>Send messagge</button>
                            </div>
                        </div>
                        <div class="success-box">
                            <div class="alert alert-success">Congratulations. Your message has been sent successfully</div>
                        </div>
                        <div class="error-box">
                            <div class="alert alert-warning">Error, please retry. Your message has not been sent</div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <hr class="space visible-xs" /> 
                    <h2>How to reach us</h2>
                    <p class="text-color no-margins text-bold">You can reach by follow the directions</p>
                    <hr class="space s" />
                    <div class="row">
                     
                        <div class="col-md-6">
                            <ul class="fa-ul">
                                <li>
                                    <i class="fa-li fa fa-home"></i>
                                    Suite A, New Block3, Sam Levy Village,
                                    Borrowdale, Harare, Zimbabwe.
                                
                                </li>
                                <!-- <li>
                                    <i class="fa-li fa fa-home"></i>
                                   Studio Massimo
                                    PO Box 16120, Collins Street West,
                                    Victoria 8007, United States.
                                </li> -->
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="fa-ul">
                                <li><i class="fa-li fa fa-phone"></i> (123) 0 123 455669</li>
                                <li><i class="fa-li fa fa-fax"></i> +263-242-885-029</li>
                                <li><i class="fa-li fa fa-fax"></i> +263-864-411-0722</li>
                                <li><i class="fa-li fa fa-envelope-o"></i> info@inafprojects.com</li>
                                <li><i class="fa-li fa fa-envelope-o"></i> mail@inafprojects.com</li>
                            </ul>
                        </div>
                    </div>
                    <hr class="space m" />
                    <div class="btn-group social-group btn-group-icons">
                        <a target="_blank" href="#" data-social="share-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                            <i class="fa fa-facebook text-s"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                            <i class="fa fa-twitter text-s"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-google" data-toggle="tooltip" data-placement="top" title="Google+">
                            <i class="fa fa-google-plus text-s"></i>
                        </a>
                        <a target="_blank" href="#" data-social="share-linkedin" data-toggle="tooltip" data-placement="top" title="LinkedIn">
                            <i class="fa fa-linkedin text-s"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-center text-center">
                    <hr class="space" />
                    <div class="google-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3799.6726709715244!2d31.087410314881676!3d-17.760055887856357!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1931b01bac875e97%3A0xc44d790ba3145735!2sSam+Levy&#39;s+Village!5e0!3m2!1sen!2sin!4v1550921261827" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    