
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Our Customers Say's</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="index">Home</a></li>
                        <li class="active">Costomer's Says</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty">
        <div class="container content">
            <div class="row">
            <?php
            if (isset($testimonials) and $testimonials) {
                foreach ($testimonials as $testimonial) {
                ?>
                <div class="col-md-4" style="margin-bottom: 50px;">
                    <div class="advs-box niche-box-testimonails-cloud">
                        <p>
                            <?php echo $testimonial->description;?>
                        </p>
                        <div class="name-box vertical-row">
                            <i class="vertical-col fa text-l circle onlycover" style="background-image:url('<?php echo $testimonial->url . $testimonial->file_name;?>')"></i>
                            <h5 class="vertical-col subtitle"><?php echo $testimonial->name;?> <span class="subtxt"><?php echo $testimonial->designation;?></span></h5>
                        </div>
                    </div>
                </div>
                <?php 
                }
            }
            ?>          
            </div>
        </div>
    </div>

    