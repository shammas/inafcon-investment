
    <div class="header-base bg-cover" style="background-image: url(img/banner-1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="title-base text-left">
                        <h1>Our Services</h1>
                        <p>Things don't have to change the world to be important</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <ol class="breadcrumb b white">
                        <li><a href="index">Home</a></li>
                        <li class="active">Our Services</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="section-empty section-item">
        <div class="container content">
            <div class="row vertical-row">
                <div class="col-md-5">
                    <img src="img/serivices-1.png" alt="" />
                </div>
                <div class="col-md-7">
                    <div class="title-base text-left">
                        <hr />
                        <h2>Interior design</h2>
                        <p>Super solutions</p>
                    </div>
                    <p>
                        Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                        cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Mauris blandit aliquet elit, eget tincidunt nibh
                        pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Proin eget tortor risus.
                        Praesent sapien massa.
                    </p>
                    <hr class="space s" />
                    <div class="row vertical-row">
                        <div class="col-md-9">
                            <table class="grid-table border-table text-left">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 class="text-color text-m">Starter plan</h4>
                                            <h5>From $15.000</h5>
                                        </td>
                                        <td>
                                            <h4 class="text-color text-m">Family plan</h4>
                                            <h5>From $30.000</h5>
                                        </td>
                                        <td>
                                            <h4 class="text-color text-m">Business plan</h4>
                                            <h5>From $90.000</h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <a href="index" class="circle-button btn-border btn btn-sm nav-justified">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="space" />
            <div class="row vertical-row" data-anima="fade-bottom">
                <div class="col-md-7">
                    <div class="title-base text-left">
                        <hr />
                        <h2>Renovation and buildings</h2>
                        <p>Cheap prices</p>
                    </div>
                    <p>
                        Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                        cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Mauris blandit aliquet elit, eget tincidunt nibh
                        pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Proin eget tortor risus.
                        Praesent sapien massa, convallis neo.
                    </p>
                    <hr class="space s" />
                    <div class="row vertical-row">
                        <div class="col-md-9">
                            <table class="grid-table border-table text-left">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 class="text-color text-m">Small flat</h4>
                                            <h5>From $7.000</h5>
                                        </td>
                                        <td>
                                            <h4 class="text-color text-m">Homes</h4>
                                            <h5>From $10.000</h5>
                                        </td>
                                        <td>
                                            <h4 class="text-color text-m">Villas</h4>
                                            <h5>From $25.000</h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <a href="contact" class="circle-button btn-border btn btn-sm nav-justified">Contact us</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 text-right">
                    <img src="img/serivices-2.png" alt="" />
                </div>
            </div>
            <hr class="space" />
            <div class="row vertical-row" data-anima="fade-bottom">
                <div class="col-md-5">
                    <img src="img/serivices-1.png" alt="" />
                </div>
                <div class="col-md-7">
                    <div class="title-base text-left">
                        <hr />
                        <h2>Outdoor and gardening</h2>
                        <p>Green technologies</p>
                    </div>
                    <p>
                        Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Lorem ipsum dolor sit amet,
                        consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
                        cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Mauris blandit aliquet elit, eget tincidunt nibh
                        pulvinar a. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Sed porttitor lectus nibh. Proin eget tortor risus.
                        Praesent sapien massa, convallis a pellentesque.
                    </p>
                    <hr class="space s" />
                    <div class="row vertical-row">
                        <div class="col-md-9">
                            <table class="grid-table border-table text-left">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h4 class="text-color text-m">Small areas</h4>
                                            <h5>From $1.000</h5>
                                        </td>
                                        <td>
                                            <h4 class="text-color text-m">Medium areas</h4>
                                            <h5>From $2.000</h5>
                                        </td>
                                        <td>
                                            <h4 class="text-color text-m">Large areas</h4>
                                            <h5>From $10.000</h5>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <a href="index" class="circle-button btn-border btn btn-sm nav-justified">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   