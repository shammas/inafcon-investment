<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*_______ public route start _______*/
$route['index'] = 'Index/index';
$route['about'] = 'Index/about';
$route['clients'] = 'Index/clients';
$route['contact'] = 'Index/contact';
$route['gallery'] = 'Index/gallery';
$route['projects'] = 'Index/projects';
// $route['projects-single'] = 'Index/projects_single';
$route['projects-single/(:num)/(:any)'] = 'Index/projects_single/$1';
$route['services'] = 'Index/services';
$route['test'] = 'Index/test';
$route['testimonial'] = 'Index/testimonial';

/****Dashboard***/
$route['login'] = 'Auth/login';
$route['logout'] = 'Auth/logout';
$route['change_password'] = 'Auth/change_password';

$route['dashboard/load-user'] = 'Dashboard/load_user';
$route['dashboard/edit-user/(:num)']['POST'] = 'Auth/edit_user/$1';

$route['dashboard'] = 'Dashboard';
$route['dashboard/(:any)'] = 'Dashboard/page/$1';

$route['dashboard/testimonial/get'] = 'Testimonial_Controller';
$route['dashboard/testimonial/upload'] = 'Testimonial_Controller/upload';
$route['dashboard/testimonial/add']['post'] = 'Testimonial_Controller/store';
$route['dashboard/testimonial/edit/(:num)']['post'] = 'Testimonial_Controller/update/$1';
$route['dashboard/testimonial/delete/(:num)']['delete'] = 'Testimonial_Controller/delete/$1';

$route['dashboard/client/get'] = 'Client_Controller';
$route['dashboard/client/upload'] = 'Client_Controller/upload';
$route['dashboard/client/add']['post'] = 'Client_Controller/store';
$route['dashboard/client/edit/(:num)']['post'] = 'Client_Controller/update/$1';
$route['dashboard/client/delete/(:num)']['delete'] = 'Client_Controller/delete/$1';

$route['dashboard/gallery_cat/get'] = 'Gallery_cat_Controller/index';
$route['dashboard/gallery_cat/add']['post'] = 'Gallery_cat_Controller/store';
$route['dashboard/gallery_cat/edit/(:num)']['post'] = 'Gallery_cat_Controller/update/$1';
$route['dashboard/gallery_cat/delete/(:num)']['delete'] = 'Gallery_cat_Controller/delete/$1';

$route['dashboard/gallery/get'] = 'Gallery_Controller';
$route['dashboard/gallery/upload'] = 'Gallery_Controller/upload';
$route['dashboard/gallery/add']['post'] = 'Gallery_Controller/store';
$route['dashboard/gallery/edit/(:num)']['post'] = 'Gallery_Controller/update/$1';
$route['dashboard/gallery/delete/(:num)']['delete'] = 'Gallery_Controller/delete/$1';

$route['dashboard/project_cat/get'] = 'Project_cat_Controller/index';
$route['dashboard/project_cat/add']['post'] = 'Project_cat_Controller/store';
$route['dashboard/project_cat/edit/(:num)']['post'] = 'Project_cat_Controller/update/$1';
$route['dashboard/project_cat/delete/(:num)']['delete'] = 'Project_cat_Controller/delete/$1';

$route['dashboard/project/upload'] = 'Project_Controller/upload';
$route['dashboard/project/get']['get'] = 'Project_Controller';
$route['dashboard/project/add']['post'] = 'Project_Controller/store';
$route['dashboard/project/edit/(:num)']['post'] = 'Project_Controller/update/$1';
$route['dashboard/project/delete-image/(:num)']['delete'] = 'Project_Controller/delete_image/$1';
$route['dashboard/project/delete/(:num)']['delete'] = 'Project_Controller/delete/$1';