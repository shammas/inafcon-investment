<?php
/**
 * 002_initial_schema.php
 * Date: 02/03/19
 * Time: 10:50 AM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {
        /**
         * Table structure for table 'testimonials'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'designation' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('testimonials');


         /**
         * Table structure for table 'clients'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('clients');

         /**
         * Table structure for table 'gallery_cats'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'gallery_cat' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('gallery_cats');

         /**
         * Table structure for table 'galleries'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'category_id' => [
                 'type'       => 'MEDIUMINT',
                 'constraint' => '8',
                 'unsigned'   => TRUE
             ],
            'video_url' => [
                 'type' => 'LONGTEXT',
                 'null' => TRUE
             ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('galleries');


         /**
         * Table structure for table 'project_cats'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'project_cat' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('project_cats');

         /**
         * Table structure for table 'projects'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'title' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'category_id' => [
                 'type'       => 'MEDIUMINT',
                 'constraint' => '8',
                 'unsigned'   => TRUE
             ],
            'description' => [
                 'type' => 'LONGTEXT',
                 'null' => TRUE
             ],
            'client' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'date' => [
                'type'=>'DATE',
                'NULL'=>TRUE,
            ],
            'place' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('projects');

        /**
         * Table structure for table 'project_files'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'project_id' => [
                'type'       => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned'   => TRUE
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('project_files'); 

        
        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('testimonials', TRUE);
        $this->dbforge->drop_table('clients', TRUE);
        $this->dbforge->drop_table('projects',TRUE);
        $this->dbforge->drop_table('project_cats', TRUE);
        $this->dbforge->drop_table('project_files',TRUE);
        $this->dbforge->drop_table('galleries',TRUE);
        $this->dbforge->drop_table('gallery_cats', TRUE);
    }
}