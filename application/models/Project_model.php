<?php
/**
 * Project_model.php
 * Date: 04/03/19
 * Time: 10:10 AM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Project_model extends MY_Model
{

    function __construct()
    {
        $this->has_many['files'] = array(
            'foreign_model' => 'project_file_model',
            'foreign_table' => 'project_files',
            'foreign_key' => 'project_id',
            'local_key' => 'id'
        ); 
        $this->has_one['project_cat'] = array(
            'foreign_model' => 'Project_cat_model',
             'foreign_table' => 'project_cats', 
             'foreign_key' => 'id', 
             'local_key' => 'category_id'
        );
         
        parent::__construct();
        $this->timestamps = TRUE;
    }
    public function next($id)
    {

        $this->db->select('*');
        $this->db->where("id = (select min(id) from projects where id > $id)");
        $query = $this->db->get('projects');
        if($query->num_rows() > 0 ){
            return $query->row();
        }
        else{
            return false;
        }
    }

     public function previous($id)
    {

        $this->db->select('*');
        $this->db->where("id = (select max(id) from projects where id < $id)");
        $query = $this->db->get('projects');
        if($query->num_rows() > 0 ){
            return $query->row();
        }
        else{
            return false;
        }
    }    

}
            