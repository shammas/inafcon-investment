<?php
/**
 * Client_model.php
 * Date: 02/03/19
 * Time: 12:31 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Client_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}