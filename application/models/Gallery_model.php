<?php
/**
 * Gallery_model.php
 * Date: 02/03/19
 * Time: 04:15 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Gallery_model extends MY_Model
{

    function __construct()
    {
        $this->has_one['gallery_cat'] = array(
            'foreign_model' => 'Gallery_cat_model',
             'foreign_table' => 'gallery_cats', 
             'foreign_key' => 'id', 
             'local_key' => 'category_id'
        );

        parent::__construct();
        $this->timestamps = TRUE;
    }

}