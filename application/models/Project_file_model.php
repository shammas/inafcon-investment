<?php
/**
 * project_file_model.php
 * Date: 04/03/19
 * Time: 10:11 AM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class project_file_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}