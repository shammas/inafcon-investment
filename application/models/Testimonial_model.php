<?php
/**
 * Testimonial_model.php
 * Date: 02/03/19
 * Time: 11:47 AM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Testimonial_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}